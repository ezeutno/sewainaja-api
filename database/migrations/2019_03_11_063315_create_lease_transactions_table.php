<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaseTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lease_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('consumer_id');
            $table->unsignedBigInteger('provider_id');
            $table->double('transaction_price',13,2);
            $table->unsignedBigInteger('transaction_delivery');
            $table->string('transaction_status');
            $table->dateTime('payment_date_time');
            $table->dateTime('lease_start_date');
            $table->dateTime('lease_end_date');
            $table->timestamps();
            $table->foreign('consumer_id')->references('id')->on('users');
            $table->foreign('provider_id')->references('id')->on('users');
            $table->foreign('transaction_delivery')->references('id')->on('deliveries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lease_transactions');
    }
}

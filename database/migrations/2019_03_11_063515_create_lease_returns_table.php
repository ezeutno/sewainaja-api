<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaseReturnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lease_returns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('lease_transaction_id');
            $table->unsignedBigInteger('return_delivery_id');
            $table->dateTime('lease_return_date');
            $table->integer('lease_return_status');
            $table->timestamps();
            $table->foreign('lease_transaction_id')->references('id')->on('lease_transactions');
            $table->foreign('return_delivery_id')->references('id')->on('deliveries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lease_returns');
    }
}

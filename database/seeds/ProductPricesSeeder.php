<?php

use App\ProductPrices;
use Illuminate\Database\Seeder;

class ProductPricesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $arrayValues = ['daily','weekly','monthly','yearly'];
        for ($i =0; $i <50; $i++){
            ProductPrices::create([
                'product_id' => rand(2,48),
                'product_price' => rand(200000,300000000),
                'product_price_type' => $arrayValues[rand(0,3)]
            ]);
        }
    }
}

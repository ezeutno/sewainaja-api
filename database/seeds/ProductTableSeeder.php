<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $arrayValues = ['alive','suspended','blocked'];
        for ($i =0; $i <50; $i++){
            Product::create([
                'user_id' => rand(2,48),
                'name' => $faker->name,
                'stock' => rand(4,1200),
                'description' => $faker->text,
                'penalty_fee' => rand(200000,3000000),
                'status' => $arrayValues[rand(0,2)]
            ]);
        }
    }
}

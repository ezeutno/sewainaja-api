<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPrices extends Model
{
    protected $table = 'product_prices';
    protected $fillable = ['product_id','product_price','product_price_type'];
}

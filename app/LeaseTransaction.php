<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaseTransaction extends Model
{
    protected $table = 'lease_transactions';
    protected $fillable = ['consumer_id','provider_id','transaction_price','transaction_delivery','transaction_status','paytime_date_time','lease_start_date','lease_end_date'];
    
    public function consumer(){
        return $this->belongsTo('App\User','consumer_id');
    }
    public function provider(){
        return $this->belongsTo('App\User','provider_id');
    }
    public function transactionDelivery(){
        return $this->belongsTo('App\Delivery','transaction_delivery');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaseReturn extends Model
{
    protected $table = 'lease_returns';
    protected $fillable = ['lease_transaction_id','return_delivery_id','lease_return_date','lease_return_status',];

    public function leaseTransaction(){
        return $this->belongsTo('App\LeaseTransaction','lease_transaction_id');
    
}

<?php

namespace App\Http\Requests;

use App\Services\Response;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

trait ApiRequest
{
    public function authorize() {
        return true;
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(Response::validationError($validator->errors()));
    }
}

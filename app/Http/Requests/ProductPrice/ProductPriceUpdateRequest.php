<?php

namespace App\Http\Requests\ProductPrice;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductPriceUpdateRequest extends FormRequest
{
    use ApiRequest;

    public function rules() {
        return [
            'product_prices' => 'integer',
            'product_price_type' => Rule::in([
                    'daily', 'weekly', 'monthly', 'yearly'
            ])
        ];
    }
}

<?php

namespace App\Http\Requests\ProductPrice;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductPriceStoreRequest extends FormRequest
{
    use ApiRequest;

    public function rules() {
        return [
            'product_id' => 'required|exists:products,id',
            'product_prices' => 'required|integer',
            'product_price_type' => [
                'required',
                Rule::in([
                    'daily', 'weekly', 'monthly', 'yearly'
                ])
            ]
        ];
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\ProductStoreRequest;
use App\Http\Requests\Product\ProductUpdateRequest;
use App\Product;
use App\Services\Response;

class ProductController extends Controller
{
    private $product;

    public function __construct(Product $product){
        $this->product = $product;
    }

    public function index(){
        $data = $this->product->all();

        return Response::data($data);
    }

    public function show($id){
        $data = $this->product->find($id);

        return Response::data($data);
    }

    public function store(ProductStoreRequest $request) {
        $params = $request->toArray();

        $this->product->create($params);

        return Response::message('Create product success');
    }

    public function update(ProductUpdateRequest $request, $id) {
        $params = $request->except('user_id');

        $product = $this->product->find($id);

        $product->fill($params);
        $product->save();

        return Response::message('Update product success');
    }

    public function destroy($id) {
        $product = $this->product->find($id);

        $product->fill([
            'status' => 'blocked'
        ]);
        $product->save();

        return Response::message('Delete product success');
    }
}

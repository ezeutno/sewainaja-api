<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProductPrice\ProductPriceStoreRequest;
use App\Http\Requests\ProductPrice\ProductPriceUpdateRequest;
use App\ProductPrices;
use App\Services\Response;

class ProductPricesController extends Controller
{
    private $productprices;
    public function __construct(ProductPrices $productprices){
        $this->productprices = $productprices;
    }
    public function index($productId){
        $data = $this->productprices->where('product_id', '=', $productId)->get();
        return Response::data($data);
    }
    public function show($productId, $id){
        $data = $this->productprices->find($id);
        return Response::data($data);
    }
    public function store(ProductPriceStoreRequest $request){
        $params = $request->toArray();
        $this->productprices->create($params);
        return Response::message('Product Price Added');
    }

    public function update(ProductPriceUpdateRequest $request,$productId, $id){
        $params = $request->except('product_id');
        $product = $this->productprices->find($id);
        $product->fill($params);
        $product->save();
        return Response::message('Update Product Price success');
    }

    public function destroy($id){
        $product = $this->product->find($id);
        $product->delete();
        return Response::massage('Delete Price Succesfull');
    }

}

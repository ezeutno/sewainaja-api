<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['user_id','name','stock','description','penalty_fee','status'];

    public function user(){
        return $this->belongsTo('App\Product','user_id');
    }
    public function productPrices(){
        return $this->hasMany('App\ProductPrices','product_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseTransaction extends Model
{
    protected $table = 'purchase_transaction';
    protected $fillable = ['lease_transaction_id','transaction_date','transaction_price','payment_date_time'];

    public function leasetransaction(){
        return $this->belongsTo('App\LeaseTransaction','lease_transaction_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaseTransactionDetail extends Model
{
    protected $table = 'lease_transaction_details';
    protected $fillable = ['lease_transaction_id','product_id','product_quantity','product_price'];

    public function leaseTransactiondetail(){
        return $this->belongsTo('App\LeaseTransaction','lease_transaction_details_id');
    }
    public function leaseTransaction(){
        return $this->belongsTo('App\Product','lease_transaction_id');
    }
}

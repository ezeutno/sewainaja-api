<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Authentication
Route::group(['prefix' => 'auth'], function() {
    Route::post('login', 'AuthController@login');

    Route::group(['middleware' => ['jwt.auth']], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('me', 'AuthController@me');
    });
});

//Read
Route::resource('products','ProductController');
Route::resource('products/{id}/prices','ProductPricesController');
//Route::resource('product/{id}/images');

